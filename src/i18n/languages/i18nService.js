/**
 * Translation service
 * How to use:
 * 1. import translations from '../../../../services/generic/i18nService';
 * 2. let stringToTranslate = translations('starter.welcome');
 */

import { LANGUAGES, DEFAULT_LANGUAGE } from '../../i18n';

const getCurrentLanguage = (() => {
  let lang = [];
  return () => {
    if (!lang.length) {
      const html = document.querySelector('html');
      if (html && html.hasAttribute('lang')) {
        lang = [html.getAttribute('lang')];
      }
      const meta = document.querySelector('meta[http-equiv=Content-Language]');
      if (meta) {
        const content = meta.getAttribute('content');
        if (content) {
          lang = [content.split('-')[0], content];
        }
      }
      if (console && console.debug) {
        console.debug(`lang=[${lang.join(', ')}]`);
      }
    }
    return lang;
  };
})();

/**
 * Service function for translating strings to the current language
 */
export default function translate(id, params) {
  const lang = getCurrentLanguage();
  const languages = LANGUAGES[lang[1]] || LANGUAGES[lang[0]];
  let out = (languages && languages[id]) || LANGUAGES[DEFAULT_LANGUAGE][id];
  if (typeof out === 'object') {
    out = out[window.location.hostname];
  }
  if (params) {
    return out.replace(/{([^}]+)}/g, (_, key) => params[key] || '');
  }
  return out || 'Translation missing';
}
