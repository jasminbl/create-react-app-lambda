/**
 * Import all language files and make them available
 */

import deDE from './languages/de-DE';
import enGB from './languages/en-GB';
import enUS from './languages/en-US';
import esES from './languages/es-ES';
import esMX from './languages/es-MX';
import frFR from './languages/fr-FR';
import huHU from './languages/hu-HU';
import itIT from './languages/it-IT';
import nlNL from './languages/nl-NL';
import ptBR from './languages/pt-BR';
import zhHant from './languages/zh-Hant';
import plPl from './languages/pl-PL';

export const LANGUAGES = {
  'de-DE': deDE,
   'en-GB': enGB,
  'en-US': enUS,
  'es-ES': esES,
  'es-MX': esMX,
  'fr-FR': frFR,
  'hu-HU': huHU,
  'it-IT': itIT,
  'nl-NL': nlNL,
  'pt-BR': ptBR,
  'zh-Hant': zhHant,
  'pl-PL': plPl
};

export const DEFAULT_LANGUAGE = 'en-GB';
