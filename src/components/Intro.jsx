import React from 'react'
import './Intro.scss';
// import me from '../assets/me.png';

// import translations from '../i18n/languages/i18nService';


function Intro() {
    // let translatedIntro = translations('intro.title');

    // let position = 0;

    // function imgMove(){
    //     document.getElementById('Layer_1')
    // }
    return (
        <div className="content content-intro">
            {/* <span>{translatedIntro} how about starting a nice projet together!</span> */}
            <span className='jaazmeyn'> Jasmin Blanda </span>
            <div className="content-intro-wrapper">
                <div className="intro-text">
                    <span className='intro-text__first'> Frontend-development </span>
                    <span className='intro-text__second'> I make your website, webshop or webapp! </span>
                    <span className='intro-text__third'> Additional services:<br></br> Concepting - Designing - Content creation </span>
                </div>
                <span className='intro-text__small'> Hello, nice having you on my site! <br></br>I am a frontenddeveloper with focus on the creative site from responsive webapps!
                    <br></br> <br></br>
                    JavaScript | HTML5 | CSS3 &amp; SCSS | Node.js | electron. | React | React.native | REST-API | JSON | GIT | Content-Management: Wordpress | Adobe Cloud | Figma | Shopify | .
                    I'm always happy about new contacts and possibilities.
                    Don't hesitate to contact me.
                    Let's create something. If you need a co-founder, lets talk!<br></br><br></br>
                    Cheers Jasmin</span>
                <div className="intro-Img">
                    {/* <img alt="me" src={me} width="200" className="me"/> */}
                  {/* <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 163.7 180.43"><title>face</title><path d="M369.05,94.15l8.9,17.08L376.17,88.1,362.29,74.22,345.57,62.83l17.79.71L350.55,51.8l-17.8-3.2H317.1l18.5-6.41-11-4.27L306.78,36.5l-8.55,3.91,4.63-8.54L291.12,33.3l-14.59,3.2-47,26.33-2.49,10v9.25l-3.56,3.56-9.25,19.22,2.85,6h10l.36,6.76h12.81l14.94-6.76L254.11,124l-4.63,6.41L231,137.57v9.6l3.91,2.5L257,151.44H268.7L280,150v8.54L276.17,166l3.83,7.82L252.33,191.3H230.62l20.28,14.59h24.56l19.22-6.4V212.3l18.15-5,15.65-11,14.59-13.87,3.21,13.87,18.5-13.87L378,152.16,374,129.38Zm-124.2-11V76.36L252,80.27Z" transform="translate(-214.25 -31.87)"/></svg> */}
                </div>
            </div>
            

            <div className="intro-grid">
                {/* <svg className="intro-Img" src="../assets/face.svg" alt="face"/> */}
            </div>
        </div>
    )
}

export default Intro
