// import Card from './Card';
// import './StripeTabs.module.scss';

// import {Link} from 'react-router-dom';
import React, {useState} from 'react';

// im foto element
const StripeTabs=(props) => {
  const [currentTab, setCurrentTab] = useState('tab1');

  const tabList = [];

  if(props.eachStripe.tabs.length > 1){
    // console.log('mehr als > 1');
    props.eachStripe.tabs.forEach((onetab)=>{
      tabList.push(onetab);
      // console.log(tabList, onetab + 'tablist over1 pushed')
    });
  } else if(props.eachStripe.tabs.length === 1){
    tabList.push(props.eachStripe.tabs)
    // console.log(props.eachStripe.tabs, '1pushed')

  } else {
    // console.log(props.eachStripe.tabs.length,'no Tabs')
  }

  return (
    <>
        <div className="tabs">
            {tabList.map((tab, i) => (
              
              // button onclick currenttab wird auf tabnamen gesetzt
                <button 
                  key={i}
                  onClick={() => setCurrentTab(tab.name)} 
                  className={(tab.name === currentTab) ? 'cathegoryTab activer' : 'cathegoryTab'}
                >
                    {/* <svg className="tab-icon" width="20" height="20">
                        <use href={tab.img}></use>
                    </svg> */}
                    <figure className="tab-icon">
                      {tab.icon}
                    </figure>
                    <div className="tab-text">{tab.label}</div>
                </button>
            ))
            }
        </div>
        {tabList.map((tab, i) => {
            if(tab.name === currentTab) {
                return <div key={i}>{tab.content}</div>; // Cards
            } else {
                return null;
            }
        })}
      </>
  )
}

export default StripeTabs;
