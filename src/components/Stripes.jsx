import React from 'react';

import './Stripe.modules.scss';
import toggleSkillStripes from '../functions/toggleSkillstripes';
// pro cathegorie 1 stripe drinnen categorie inhalt
import StripeTabs from './StripeTabs';
// import { "#magnifying-glass" } from 'react-icons/fa';
import sprite from '../assets/sprite.svg';

// import {IconContext} from 'react-icons';
// https://react-icons.github.io/react-icons/icons?name=fa
import { FaPepperHot } from 'react-icons/fa';
import { FaGlassCheers } from "react-icons/fa";
import { FaHamburger } from "react-icons/fa";
import { FaGrinTongueWink } from "react-icons/fa";
import { FaHeadphones } from "react-icons/fa";
// import { FaMicrophone } from "react-icons/fa";
// import { FaPalette } from "react-icons/fa";
import { FaPhotoVideo } from "react-icons/fa";
import { RiTShirt2Fill } from "react-icons/ri";
import {BiRightArrow} from "react-icons/bi"; 

import Shop from '../templates/Shop';

//eventfotoimports!
import FotoAlbum from '../templates/FotoAlbum';

const Data =  [
    {
        id: 'asdfgh',
        stripetitle:'Foto',
        tabTitle:'Foto',
        tabs:[
            {
                name: 'Event Fotos',
                label: 'Event Fotos',
                icon: <FaGlassCheers />,
                img: sprite + '#magnifying-glass',
                content: (
                    <div className="tab-content eventfoto-card-area">
                      <h2>Event fotos - Additional service</h2><br></br>
                      <p>Here are the latest top free picks from the last event! Fits to you Landingpage? Musicians Gallerys Workshops </p>
                      <FotoAlbum />
                    </div>
                )
            }, 
            {
                name: 'Food Fotografie',
                label: 'Food Fotografie',
                icon: <FaHamburger />,
                // img: sprite + '#magnifying-glass',

                content: (
                    <div className="tab-content">
                        <h2>Foodstyling - Additional service - content for your Site</h2>
                        <p>If you sell cookbooks, this could be perfect to book additionally to your advertisement Landing Page or food app some Photos.</p>
                    </div>
                )
            }, 
        ],
    },

   {
        id: 'asdfgmh',
        stripetitle:'Art',
        tabTitle:['Art', 'paintings'],
        tabs:[
            {
                name: 'illustrations',
                label: 'illustrations',
                icon: <FaPepperHot />,
                img: sprite + '#magnifying-glass',
                content: (
                    <div className="tab-content">
                        <h2>Something I did</h2>
                        <p>Here is some text and will be a art Gallery as well</p>
                        <p>Lorem ipsum dolor sit amet ...</p>
                    </div>
                )
            }, 
            {
                name: 'compositions',
                label: 'compositions',
                icon: <FaGrinTongueWink />,
                img: sprite + '#magnifying-glass',
                content: (
                    <div className="tab-content">
                        <h2>Something else I did</h2>
                        <p>Here is some text and will be a art Gallery as well</p>
                        <p>Lorem ipsum dolor sit amet ...</p>
                    </div>
                )
            }, 
            {
                name: 'NFTs',
                label: 'NFTs',
                icon: <FaHeadphones />,
                img: sprite + '#magnifying-glass',
                content: (
                    <div className="tab-content">
                        <h2>Brand new Collection!</h2>
                        <p>See my last work here</p>
                        <p>Lorem ipsum dolor sit amet ...</p>
                    </div>
                )
            }, 
        ],
    },

    {
        id: 'asdjfgh',
        stripetitle:'Design',
        tabTitle:['Design', 'paintings'],
        tabs:[
            {
                name: 'shirts',
                label: 'CI & CD',
                icon: <RiTShirt2Fill />,
                img: sprite + '#magnifying-glass',
                content: (
                    <Shop />
                )
            }, 
            {
                name: 'UX/UI prototype styletyle',
                label: 'UX/UI prototype styletyle',
                icon: <FaPhotoVideo />,
                content: (
                    <div className="tab-content">
                        <h2>Graphic design </h2>
                        <p>Some other content  of my workexperience </p>
                        <p>in Graphic design comes soon</p>
                    </div>
                )
            }, 
        ],
    }

]

const Stripes = () => {

    function generateUniqueID () {
        return `${Date.now()}-${Math.floor(Math.random() * (9e12 - 1)) + 1e12}`;
    }

    return (
        <div className='content content-toggle'>
          { Data.map((eachStripe) => { // .opener-content
            return (
                <div className='toggle-stripe' onClick={toggleSkillStripes} key={generateUniqueID()}>
                    <div className='opener' >
                        <p className='p'>{eachStripe.stripetitle}</p> {/* each -> 'Angula', einzeln -> 'CI/CD' */}
                        <BiRightArrow className="codesymbol" alt={eachStripe.title} />
                        {/* <img src='https://octily.studio/csod/henkel/dxcustompages/assets/icon/svg/Icon_Arrow-back.svg' className='codesymbol' alt={eachStripe.title} width="16.34"/> */}
                    </div>
                    <div className='opener-content'>
                        <StripeTabs onClick={null} eachStripe={eachStripe}/>
                    </div>
                </div>
            );
          })}
        </div>
    )
}
export default Stripes;