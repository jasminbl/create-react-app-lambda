import React from 'react'

function Shopelement (props) {
    console.log(props.id)
    return (
        <>
          <img src={props.img} alt="product-img"/>
          <h1 key={ props.id } >{props.title}</h1>
          <p> {props.price}</p>
        </>  
    )
}

export default Shopelement
