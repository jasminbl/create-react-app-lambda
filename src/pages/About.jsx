import React from 'react'
import './About.scss';

import cv from '../assets/jasmin_blanda_dev_cv_en.pdf'

function About() {
    return (
        <div className="cv">
            <object data={cv} className="cv" width="210" height="297">
                <a href={cv} title="cv">laden</a>
            </object>
        </div>
    )
}

export default About
