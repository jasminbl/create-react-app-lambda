import React from 'react'
import Shopelement from '../components/Shopelement';
import {storeProducts as Products} from '../data/shopData';

function ArtProductOverview (){
    console.log(Products);
        return(
            Products.map(each => {
                return(
                   <Shopelement
                      img={each.img}
                      key={each.id}
                      title={each.title}
                      price={each.price}/>
                )
            })
        )
}



function Shop() {
    return (
        <>
        <div></div>
        <ArtProductOverview />
        </>
    )
}







export default Shop
