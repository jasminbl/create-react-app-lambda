import React from 'react'

import Stripes from '../components/Stripes';
import Intro from '../components/Intro';

function Main (){
        return(
            <>
                <Intro />
                <Stripes />

            </>
        );
}

export default Main;
