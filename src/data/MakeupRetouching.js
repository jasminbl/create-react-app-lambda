export const MakeupRetouching = {
      'Events': {
        'icon': 'EventIcon',
        'title': 'Events',
        'teamsChannelUrl': 'https://teams.microsoft.com/l/channel/19%3a224ff473275644da956796ccdb3dd2d4%40thread.tacv2/Programming%2520Community?groupId=97fad842-7d76-473a-8d42-3eb0727517a6&tenantId=e8701075-0d9e-4ea1-991d-5a0d110a5d29',
        'playlists': [
          {
            title: "Set am Brett",
            id: '21ce1355-2848-4e8d-953a-c3f30d300b4c',
            date: '14.09.20',
            img: "img/product-1.png",
            price: 10,
            company: "GOOGLE",
            info:
                "Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
            inCart: false,
            count: 0,
            total: 0
          },
          {
            title: "Take over, sportevents, ",
            id: 'e48bf78e-832b-4f42-acdd-056050045cd1',
            date: '14.09.20',
            img: "img/product-1.png",
            price: 10,
            company: "GOOGLE",
            info:
            "Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
            inCart: false,
            count: 0,
            total: 0
          },
        ]
      },
      'FotoProjects, makeup, constellations': {
        'icon': 'MakupIcon',
        'title': 'Cloud',
        'teamsChannelUrl': 'https://teams.microsoft.com/l/channel/19%3a165de41760cd429db5cc46f81700d85c%40thread.tacv2/Cloud%2520Community?groupId=97fad842-7d76-473a-8d42-3eb0727517a6&tenantId=e8701075-0d9e-4ea1-991d-5a0d110a5d29',
        'playlists': [
          {
            title: 'Makeup ',
            id: '0b3edb7a-8031-427a-9c00-e9f469c9adcc',
            date: '14.09.20',
            img: "img/product-1.png",
            price: 10,
            company: "GOOGLE",
            info:
            "Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
            inCart: false,
            count: 0,
            total: 0
          },
        ]
      },
      'PS 3d?': {
        'icon': 'ItIcon',
        'title': 'IT Processes',
        'teamsChannelUrl': 'https://teams.microsoft.com/l/channel/19%3a2754742b7687451f96bbd162bf01dfd2%40thread.tacv2/IT%2520Process%2520Community?groupId=97fad842-7d76-473a-8d42-3eb0727517a6&tenantId=e8701075-0d9e-4ea1-991d-5a0d110a5d29',
        'playlists': [
          {
            title: 'Access Management',
            id: '0ef7d585-2c8b-4559-84d9-45087aa5386b'
          },
        ]
      },
    };

    