import React, {useEffect, useState } from 'react';
import './FotoAlbum.scss';
import XMLParser from 'react-xml-parser';
    // Flickr key 11e2577d3e1ba735dcee876ef4822282
    // secret key 3935e5be6aad471f 

function FotoAlbum () {
    const API_KEY = '11e2577d3e1ba735dcee876ef4822282';

    var PhotosetUrl = `https://www.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=${API_KEY}&photoset_id=72157720061954379&user_id=194213589@N07`; //oder 194213589@N07

    var requestOptions = {
        method: 'GET',
    };
    //https://www.flickr.com/services/api/explore/flickr.photosets.getPhotos
    // works _> let TestimgUrl = `https://live.staticflickr.com/65535/51623294276_4f5d542b6e.jpg`;
    
    const [album, setAlbum]  = useState();
    const [error, setError] = useState(false);
    const [loading, setLoader] = useState(true);



    const loadData = () => {        
        setLoader(true);

        fetch( PhotosetUrl, requestOptions )
        .then(response => {
            
            if (!response.ok){
                throw Error('ERROR')
            };
            
            return response.text();

        }).then(data => {
            var xml = new XMLParser().parseFromString(data); 

            // console.log(xml.children[0].children,'xml')
            
            setAlbum(xml.children[0].children);
            setLoader(false);

        }).catch(error => {
            setError(true);
            // setAlbum();
            console.log('error', error);
        })// catch

    }; //loaddata

    useEffect(() => {
        loadData();
    },[]);

    if(loading){
       return( <div>loading</div>);
       
    }
    console.log(album, 'album');

    if(error){
        return(<div>error</div>)
    }

    return (
        <ul>{
            album?.map( (image) => {
                console.log(image.attributes.id, 'it works!!!', image.attributes.farm);
                let ImgUrl = `https://live.staticflickr.com/${image.attributes.server}/${image.attributes.id}_${image.attributes.secret}.jpg`;
                
                    return (
                        <li key={image.attributes.id}>
                            <div className="hovereventCard">
                                {/* <div className="title">{image.attributes.title}</div> */}
                                <p className="date">Pick the photo from 12.11.2020 for free to your site!</p>
                                <div className="locationicon"></div>
                            </div>  
                            <img src={ImgUrl} alt='img' id={image.attributes.id}/> 
                        </li>
                    ) // return
            }) // foreach 2
        }</ul>
    ) // return
} //Eventfoto

export default FotoAlbum;
