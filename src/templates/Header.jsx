import React from 'react'
import './Header.scss';

function Header() {
    return (
        <div className="header">
            <ul>
                {/* <li><a href="/shop">Shop</a></li> */}
                <li><a href="/about">About</a></li>
                <li><a href="/portfolio">Portfolio</a></li>
            </ul>
        </div>
    )
}

export default Header
