import React from 'react';
import FotoAlbum from './FotoAlbum';
import {Router, Route} from 'react-router-dom';

/*
 * @returns Tabinhalt(Eventuebersicht) out of Albumcomponents??(eachevent)
 */
function Collection() {
    // 1. fetch all Collections from my flickr
    // 2. find each collection.name contains a tab name 1COLLECTION === 1TAB
    // 3. if its true (eventfotos is in the data OBJECT for my tabs(my programming) && (flickr has this same collectionname btw it exists))
    // --> then render Albumcomponents inside the tab inside the collection component

    let collection = ['id1, id2'];
    let collectionId = '72157720061954379';
    //https://www.flickr.com/photos/{user-id}/albums/ - all albums

    return (
        <Router>{
            collection?.map( (album) => {
                    return (
                        <>
                        <div className="hovereventCard">
                            <div className="title">{album.attributes.title}</div>
                            <p>12.11.2020</p>
                            <div className="locationicon"></div>
                        </div> 
                        {/* EVENTUEBERSICHT */}
                        <Route exact path={collectionId} 
                        key={album.attributes.id} 
                        render = { props => (
                            // albumname img with first image of album by default: let ImgUrl = `https://live.staticflickr.com/${image.attributes.server}/${image.attributes.id}_${image.attributes.secret}.jpg`;
                            // if one has lable hero take this
                            // onclick
                            // album name
                            // album date
                            // img aus album mit dem tag cover. oder irgendwie defienieren welches bild innerhalb des albums das coverbild sein soll.
                            // onclick render FotoAlbum:
                            <FotoAlbum {...props}/>
                        )} />
                        {/* <img src={AlbumImgUrl} alt='img' id={album.attributes.id}/>  */}
                       </>
                    ) // return
            }) // foreach 2
        }</Router>
    )
}

export default Collection
