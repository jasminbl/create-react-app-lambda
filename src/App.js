import React from "react"
import Header from './templates/Header'
import Footer from './templates/Footer'
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Shop from './pages/Shop'
import About from './pages/About'

import PortfolioPage from './pages/PortfolioPage'
import Main from './pages/Main';
import './sass/_layout.scss';

const Page =()=>{
  return(
    <Router>
      {/* <Route exact path='/' handleLogin={handleLogin}  */}
      <Route exact path='/' 
        render = { props => (
          <PortfolioPage {...props}/>
        )} 
      />
      <Route exact path="/main"
        render = { props => (
          <Main {...props} />
        )} 
      />
      <Route exact path="/shop"
        render = { props => (
          <Shop {...props} />
        )} 
      />

      <Route exact path="/about"
        render = { props => (
          <About {...props} />
        )} 
      />

      <Route exact path="/portfolio" component={PortfolioPage} />
      {/* <Route exact path="/about" component={AboutPage} /> */}
    </Router>
  )
}


const App = () => {  
  return (
    <div className="App">
      <Header />
      <Page />
      <Footer className="footer"/>
    </div>
  )
}

export default App
