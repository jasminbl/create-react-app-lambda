import '../components/Stripe.modules.scss';

const toggleSkillStripes = function (e) {
  let clicked = e.target;

  if(clicked.parentElement.classList.contains('opener')){

    console.log('clicked.nextSibling.classList//toggleSkillStripes')

    if (clicked.parentElement.nextSibling.classList.contains('active')) {
      clicked.parentElement.nextSibling.classList.remove('active');
      clicked.parentElement.classList.remove('active-stripe-opener');

      return;

    } else {
      console.log('cant read nextsibling')
    }
    clicked.parentElement.nextSibling.classList.add('active');
    clicked.parentElement.classList.add('active-stripe-opener');

  }
};

export default toggleSkillStripes;
